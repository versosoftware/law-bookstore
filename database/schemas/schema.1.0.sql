
CREATE TABLE locales (
                id INT AUTO_INCREMENT NOT NULL,
                name VARCHAR(150) NOT NULL,
                code VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE failed_jobs (
                id INT AUTO_INCREMENT NOT NULL,
                failed_at DATETIME NOT NULL,
                connection VARCHAR(255) NOT NULL,
                queue VARCHAR(255) NOT NULL,
                payload TEXT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE import_statuses (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                css_class VARCHAR(20) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE layouts (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150),
                slug VARCHAR(100) NOT NULL,
                label VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE pricing_types (
                id INT AUTO_INCREMENT NOT NULL,
                slug VARCHAR(30) NOT NULL,
                label VARCHAR(100) NOT NULL,
                description VARCHAR(255) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE tag_groups (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE payment_types (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                description VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE delivery_channel_types (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                label VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE record_statuses (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                description VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE workflow_statuses (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150) NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE reading_levels (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                description VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE age_restrictions (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150) NOT NULL,
                label VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE genres (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                description VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE user_types (
                id INT AUTO_INCREMENT NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                description VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE organisation_types (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                label VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX organisation_types_idx
 ON organisation_types
 ( slug, label );

CREATE TABLE organisations (
                id INT AUTO_INCREMENT NOT NULL,
                organisation_type_id INT NOT NULL,
                website VARCHAR(30),
                created_at DATETIME NOT NULL,
                active BOOLEAN DEFAULT TRUE NOT NULL,
                description VARCHAR(150),
                logo VARCHAR(80),
                name VARCHAR(100) NOT NULL,
                slug VARCHAR(50) NOT NULL,
                parent_organisation_id INT,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX organisation_idx
 ON organisations
 ( slug );

CREATE TABLE widgets (
                id INT AUTO_INCREMENT NOT NULL,
                fully_qualified_class_name VARCHAR(150) NOT NULL,
                organisation_id INT,
                label VARCHAR(50) NOT NULL,
                description VARCHAR(200) NOT NULL,
                html TEXT NOT NULL,
                parameters TEXT,
                slug VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE templates (
                id INT AUTO_INCREMENT NOT NULL,
                title VARCHAR(100) NOT NULL,
                slug VARCHAR(50) NOT NULL,
                layout_id INT NOT NULL,
                organisation_id INT,
                body TEXT NOT NULL,
                summary VARCHAR(150),
                PRIMARY KEY (id)
);


CREATE TABLE users (
                id INT AUTO_INCREMENT NOT NULL,
                last_name VARCHAR(30) NOT NULL,
                email VARCHAR(50),
                created_at DATETIME NOT NULL,
                remember_token VARCHAR(150),
                age INT NOT NULL,
                active BOOLEAN DEFAULT TRUE NOT NULL,
                city VARCHAR(100) NOT NULL,
                country VARCHAR(100) NOT NULL,
                gender CHAR(1) NOT NULL,
                password_reset_token VARCHAR(100),
                organisation_id INT,
                user_type_id INT NOT NULL,
                avatar VARCHAR(200),
                password VARCHAR(400) NOT NULL,
                facebook_user_id VARCHAR(100),
                login_token VARCHAR(300),
                msisdn VARCHAR(20) NOT NULL,
                first_name VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX users_idx
 ON users
 ( email, msisdn, login_token );

CREATE TABLE imports (
                id INT AUTO_INCREMENT NOT NULL,
                launched_by_user_id INT NOT NULL,
                created_at DATETIME NOT NULL,
                import_error_message TEXT,
                provider_path VARCHAR(255),
                rss_path VARCHAR(255),
                file_path VARCHAR(255),
                items_count INT DEFAULT 0 NOT NULL,
                naming_convention VARCHAR(150),
                process_uuid VARCHAR(100) NOT NULL,
                bulk_settings TEXT NOT NULL,
                import_status_id INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX media_imports_idx
 ON imports
 ( process_uuid );

CREATE TABLE applications (
                id INT AUTO_INCREMENT NOT NULL,
                organisation_id INT NOT NULL,
                active BOOLEAN DEFAULT TRUE NOT NULL,
                slug VARCHAR(30) NOT NULL,
                name VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE api_keys (
                id INT AUTO_INCREMENT NOT NULL,
                application_id INT NOT NULL,
                active BOOLEAN DEFAULT TRUE NOT NULL,
                created_at DATETIME NOT NULL,
                hash VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX api_keys_hash_idx
 ON api_keys
 ( hash );

CREATE TABLE media_types (
                id INT AUTO_INCREMENT NOT NULL,
                description VARCHAR(150) NOT NULL,
                label VARCHAR(50) NOT NULL,
                slug VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE tags (
                id INT AUTO_INCREMENT NOT NULL,
                slug VARCHAR(30) NOT NULL,
                tag_group_id INT NOT NULL,
                counter INT DEFAULT 0,
                label VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE categories (
                id INT AUTO_INCREMENT NOT NULL,
                slug VARCHAR(30) NOT NULL,
                name VARCHAR(100) NOT NULL,
                price DOUBLE PRECISION DEFAULT 0.00 NOT NULL,
                parent_category INT,
                PRIMARY KEY (id)
);


CREATE TABLE books (
                id INT NOT NULL,
                slug VARCHAR(50) NOT NULL,
                description VARCHAR(255) NOT NULL,
                created_at DATETIME NOT NULL,
                record_status_id INT NOT NULL,
                age_restriction_id INT NOT NULL,
                active BOOLEAN DEFAULT FALSE NOT NULL,
                organisation_id INT,
                modified_at DATETIME NOT NULL,
                workflow_status_id INT NOT NULL,
                pricing_amount DOUBLE PRECISION NOT NULL,
                reading_level_id INT NOT NULL,
                pricing_type_id INT,
                name VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE book_genres (
                current_book INT NOT NULL,
                genre_id INT NOT NULL,
                PRIMARY KEY (current_book, genre_id)
);


CREATE TABLE book_tags (
                current_book_id INT NOT NULL,
                tag_id INT NOT NULL,
                PRIMARY KEY (current_book_id, tag_id)
);


CREATE TABLE book_categories (
                book_id INT NOT NULL,
                category_id INT NOT NULL,
                PRIMARY KEY (book_id, category_id)
);


CREATE TABLE chapters (
                id INT AUTO_INCREMENT NOT NULL,
                book_id INT NOT NULL,
                name VARCHAR(100) NOT NULL,
                pricing_amount DOUBLE PRECISION NOT NULL,
                pricing_type_id INT,
                modified_at DATETIME NOT NULL,
                created_at DATETIME NOT NULL,
                slug VARCHAR(50) NOT NULL,
                description VARCHAR(255) NOT NULL,
                sort_order INT DEFAULT 0 NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE locale_chapters (
                id INT AUTO_INCREMENT NOT NULL,
                locale_id INT NOT NULL,
                name VARCHAR(100) NOT NULL,
                description VARCHAR(255) NOT NULL,
                slug VARCHAR(50) NOT NULL,
                chapter_id INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE payment_options (
                id INT AUTO_INCREMENT NOT NULL,
                book_id INT NOT NULL,
                name VARCHAR(100) NOT NULL,
                consumer_message VARCHAR(400) NOT NULL,
                renew_message VARCHAR(400),
                payment_type_id INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE settings (
                id INT AUTO_INCREMENT NOT NULL,
                setting_name VARCHAR(80) NOT NULL,
                payment_option_id INT NOT NULL,
                setting_value VARCHAR(200) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE chapter_payment_options (
                chapter_id INT NOT NULL,
                payment_option INT NOT NULL,
                PRIMARY KEY (chapter_id, payment_option)
);


CREATE TABLE delivery_channels (
                id INT AUTO_INCREMENT NOT NULL,
                book_id INT NOT NULL,
                delivery_channel_type_id INT NOT NULL,
                description VARCHAR(150) NOT NULL,
                created_at DATETIME NOT NULL,
                application_id INT,
                modified_at DATETIME NOT NULL,
                name VARCHAR(100) NOT NULL,
                slug VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE delivery_channels_payment_options (
                delivery_channel INT NOT NULL,
                payment_options_id INT NOT NULL,
                PRIMARY KEY (delivery_channel, payment_options_id)
);


CREATE TABLE pages (
                id INT AUTO_INCREMENT NOT NULL,
                content TEXT NOT NULL,
                featured_image_page_id INT,
                url VARCHAR(100) NOT NULL,
                page_media_id INT,
                label VARCHAR(100) NOT NULL,
                description VARCHAR(255) NOT NULL,
                media_id INT,
                keywords TEXT NOT NULL,
                created_at DATETIME NOT NULL,
                modified_at DATETIME NOT NULL,
                title VARCHAR(150) NOT NULL,
                PRIMARY KEY (id)
);


CREATE UNIQUE INDEX contents_idx
 ON pages
 ( url );

CREATE TABLE locale_pages (
                id INT AUTO_INCREMENT NOT NULL,
                locale_id INT NOT NULL,
                content TEXT NOT NULL,
                title VARCHAR(150) NOT NULL,
                description VARCHAR(255) NOT NULL,
                keywords TEXT NOT NULL,
                label VARCHAR(100) NOT NULL,
                url VARCHAR(100) NOT NULL,
                page_id INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE medias (
                id INT AUTO_INCREMENT NOT NULL,
                name VARCHAR(150) NOT NULL,
                path VARCHAR(450) NOT NULL,
                point_of_interest_y DOUBLE PRECISION DEFAULT 0.00 NOT NULL,
                page_id INT NOT NULL,
                parent_width VARCHAR(5),
                extension VARCHAR(20) NOT NULL,
                parent_height VARCHAR(5),
                organisation_id INT,
                point_of_interest_x DOUBLE PRECISION DEFAULT 0.00 NOT NULL,
                modified_at DATETIME NOT NULL,
                created_at DATETIME NOT NULL,
                thumbnail_path VARCHAR(450),
                medium_path VARCHAR(450),
                media_type_id INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE import_items (
                import_id INT NOT NULL,
                page_id INT NOT NULL,
                PRIMARY KEY (import_id, page_id)
);


ALTER TABLE locale_pages ADD CONSTRAINT locales_locale_pages_fk
FOREIGN KEY (locale_id)
REFERENCES locales (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE locale_chapters ADD CONSTRAINT locales_locale_chapters_fk
FOREIGN KEY (locale_id)
REFERENCES locales (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE imports ADD CONSTRAINT import_statuses_imports_fk
FOREIGN KEY (import_status_id)
REFERENCES import_statuses (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE templates ADD CONSTRAINT layouts_templates_fk
FOREIGN KEY (layout_id)
REFERENCES layouts (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT pricing_types_books_fk
FOREIGN KEY (pricing_type_id)
REFERENCES pricing_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE chapters ADD CONSTRAINT pricing_types_chapters_fk
FOREIGN KEY (pricing_type_id)
REFERENCES pricing_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tags ADD CONSTRAINT tag_categories_tags_fk
FOREIGN KEY (tag_group_id)
REFERENCES tag_groups (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE payment_options ADD CONSTRAINT payment_types_delivery_channel_payment_options_fk
FOREIGN KEY (payment_type_id)
REFERENCES payment_types (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE delivery_channels ADD CONSTRAINT delivery_channel_types_delivery_chanels_fk
FOREIGN KEY (delivery_channel_type_id)
REFERENCES delivery_channel_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT record_statuses_books_fk
FOREIGN KEY (record_status_id)
REFERENCES record_statuses (id)
ON DELETE RESTRICT
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT workflow_statuses_books_fk
FOREIGN KEY (workflow_status_id)
REFERENCES workflow_statuses (id)
ON DELETE RESTRICT
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT reading_levels_books_fk
FOREIGN KEY (reading_level_id)
REFERENCES reading_levels (id)
ON DELETE RESTRICT
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT age_restrictions_books_fk
FOREIGN KEY (age_restriction_id)
REFERENCES age_restrictions (id)
ON DELETE RESTRICT
ON UPDATE NO ACTION;

ALTER TABLE book_genres ADD CONSTRAINT genres_book_genres_fk
FOREIGN KEY (genre_id)
REFERENCES genres (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE users ADD CONSTRAINT user_types_users_fk
FOREIGN KEY (user_type_id)
REFERENCES user_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE organisations ADD CONSTRAINT organisation_type_organisation_fk
FOREIGN KEY (organisation_type_id)
REFERENCES organisation_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE organisations ADD CONSTRAINT organisation_organisation_fk
FOREIGN KEY (parent_organisation_id)
REFERENCES organisations (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE medias ADD CONSTRAINT organisation_medias_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE applications ADD CONSTRAINT organisations_applications_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE users ADD CONSTRAINT organisations_users_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE templates ADD CONSTRAINT organisations_templates_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE books ADD CONSTRAINT organisations_books_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE widgets ADD CONSTRAINT organisations_widgets_fk
FOREIGN KEY (organisation_id)
REFERENCES organisations (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE imports ADD CONSTRAINT users_media_imports_fk
FOREIGN KEY (launched_by_user_id)
REFERENCES users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE import_items ADD CONSTRAINT imports_import_items_fk
FOREIGN KEY (import_id)
REFERENCES imports (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE delivery_channels ADD CONSTRAINT applications_delivery_channels_fk
FOREIGN KEY (application_id)
REFERENCES applications (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE api_keys ADD CONSTRAINT applications_api_keys_fk
FOREIGN KEY (application_id)
REFERENCES applications (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE medias ADD CONSTRAINT media_types_medias_fk
FOREIGN KEY (media_type_id)
REFERENCES media_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE book_tags ADD CONSTRAINT tags_book_tags_fk
FOREIGN KEY (tag_id)
REFERENCES tags (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE categories ADD CONSTRAINT categories_categories_fk
FOREIGN KEY (parent_category)
REFERENCES categories (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;

ALTER TABLE book_categories ADD CONSTRAINT categories_book_categories_fk
FOREIGN KEY (category_id)
REFERENCES categories (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE delivery_channels ADD CONSTRAINT books_delivery_channels_fk
FOREIGN KEY (book_id)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE payment_options ADD CONSTRAINT books_payment_options_fk
FOREIGN KEY (book_id)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE chapters ADD CONSTRAINT books_chapters_fk
FOREIGN KEY (book_id)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE book_categories ADD CONSTRAINT books_book_categories_fk
FOREIGN KEY (book_id)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE book_tags ADD CONSTRAINT books_book_tags_fk
FOREIGN KEY (current_book_id)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE book_genres ADD CONSTRAINT books_book_genres_fk
FOREIGN KEY (current_book)
REFERENCES books (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE chapter_payment_options ADD CONSTRAINT chapters_chapter_payment_options_fk
FOREIGN KEY (chapter_id)
REFERENCES chapters (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE locale_chapters ADD CONSTRAINT chapters_locale_chapters_fk
FOREIGN KEY (chapter_id)
REFERENCES chapters (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE chapter_payment_options ADD CONSTRAINT payment_options_section_payment_options_fk
FOREIGN KEY (payment_option)
REFERENCES payment_options (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE delivery_channels_payment_options ADD CONSTRAINT payment_options_delivery_channels_payment_options_fk
FOREIGN KEY (payment_options_id)
REFERENCES payment_options (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE settings ADD CONSTRAINT payment_options_settings_fk
FOREIGN KEY (payment_option_id)
REFERENCES payment_options (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE delivery_channels_payment_options ADD CONSTRAINT delivery_channels_delivery_channels_payment_options_fk
FOREIGN KEY (delivery_channel)
REFERENCES delivery_channels (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE import_items ADD CONSTRAINT pages_import_items_fk
FOREIGN KEY (page_id)
REFERENCES pages (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE pages ADD CONSTRAINT pages_pages_fk
FOREIGN KEY (featured_image_page_id)
REFERENCES pages (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE pages ADD CONSTRAINT pages_pages_media_fk
FOREIGN KEY (page_media_id)
REFERENCES pages (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE medias ADD CONSTRAINT pages_medias_fk
FOREIGN KEY (page_id)
REFERENCES pages (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE locale_pages ADD CONSTRAINT pages_locale_pages_fk
FOREIGN KEY (page_id)
REFERENCES pages (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE pages ADD CONSTRAINT medias_pages_fk
FOREIGN KEY (media_id)
REFERENCES medias (id)
ON DELETE SET NULL
ON UPDATE NO ACTION;
