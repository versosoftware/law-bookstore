<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Books
 *
 * @ORM\Table(name="books", indexes={@ORM\Index(name="pricing_types_books_fk", columns={"pricing_type_id"}), @ORM\Index(name="record_statuses_books_fk", columns={"record_status_id"}), @ORM\Index(name="workflow_statuses_books_fk", columns={"workflow_status_id"}), @ORM\Index(name="reading_levels_books_fk", columns={"reading_level_id"}), @ORM\Index(name="age_restrictions_books_fk", columns={"age_restriction_id"}), @ORM\Index(name="organisations_books_fk", columns={"organisation_id"})})
 * @ORM\Entity
 */
class Books
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var float
     *
     * @ORM\Column(name="pricing_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $pricingAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var \App\Lib\Domain\Entities\AgeRestrictions
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\AgeRestrictions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="age_restriction_id", referencedColumnName="id")
     * })
     */
    private $ageRestriction;

    /**
     * @var \App\Lib\Domain\Entities\Organisations
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Organisations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisation_id", referencedColumnName="id")
     * })
     */
    private $organisation;

    /**
     * @var \App\Lib\Domain\Entities\PricingTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\PricingTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pricing_type_id", referencedColumnName="id")
     * })
     */
    private $pricingType;

    /**
     * @var \App\Lib\Domain\Entities\ReadingLevels
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\ReadingLevels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reading_level_id", referencedColumnName="id")
     * })
     */
    private $readingLevel;

    /**
     * @var \App\Lib\Domain\Entities\RecordStatuses
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\RecordStatuses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="record_status_id", referencedColumnName="id")
     * })
     */
    private $recordStatus;

    /**
     * @var \App\Lib\Domain\Entities\WorkflowStatuses
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\WorkflowStatuses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_status_id", referencedColumnName="id")
     * })
     */
    private $workflowStatus;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Categories", inversedBy="book")
     * @ORM\JoinTable(name="book_categories",
     *   joinColumns={
     *     @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *   }
     * )
     */
    private $category;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Genres", inversedBy="currentBook")
     * @ORM\JoinTable(name="book_genres",
     *   joinColumns={
     *     @ORM\JoinColumn(name="current_book", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     *   }
     * )
     */
    private $genre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Tags", inversedBy="currentBook")
     * @ORM\JoinTable(name="book_tags",
     *   joinColumns={
     *     @ORM\JoinColumn(name="current_book_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     *   }
     * )
     */
    private $tag;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genre = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tag = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Books
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Books
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Books
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Books
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Books
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set pricingAmount
     *
     * @param float $pricingAmount
     *
     * @return Books
     */
    public function setPricingAmount($pricingAmount)
    {
        $this->pricingAmount = $pricingAmount;

        return $this;
    }

    /**
     * Get pricingAmount
     *
     * @return float
     */
    public function getPricingAmount()
    {
        return $this->pricingAmount;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Books
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ageRestriction
     *
     * @param \App\Lib\Domain\Entities\AgeRestrictions $ageRestriction
     *
     * @return Books
     */
    public function setAgeRestriction(\App\Lib\Domain\Entities\AgeRestrictions $ageRestriction = null)
    {
        $this->ageRestriction = $ageRestriction;

        return $this;
    }

    /**
     * Get ageRestriction
     *
     * @return \App\Lib\Domain\Entities\AgeRestrictions
     */
    public function getAgeRestriction()
    {
        return $this->ageRestriction;
    }

    /**
     * Set organisation
     *
     * @param \App\Lib\Domain\Entities\Organisations $organisation
     *
     * @return Books
     */
    public function setOrganisation(\App\Lib\Domain\Entities\Organisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App\Lib\Domain\Entities\Organisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set pricingType
     *
     * @param \App\Lib\Domain\Entities\PricingTypes $pricingType
     *
     * @return Books
     */
    public function setPricingType(\App\Lib\Domain\Entities\PricingTypes $pricingType = null)
    {
        $this->pricingType = $pricingType;

        return $this;
    }

    /**
     * Get pricingType
     *
     * @return \App\Lib\Domain\Entities\PricingTypes
     */
    public function getPricingType()
    {
        return $this->pricingType;
    }

    /**
     * Set readingLevel
     *
     * @param \App\Lib\Domain\Entities\ReadingLevels $readingLevel
     *
     * @return Books
     */
    public function setReadingLevel(\App\Lib\Domain\Entities\ReadingLevels $readingLevel = null)
    {
        $this->readingLevel = $readingLevel;

        return $this;
    }

    /**
     * Get readingLevel
     *
     * @return \App\Lib\Domain\Entities\ReadingLevels
     */
    public function getReadingLevel()
    {
        return $this->readingLevel;
    }

    /**
     * Set recordStatus
     *
     * @param \App\Lib\Domain\Entities\RecordStatuses $recordStatus
     *
     * @return Books
     */
    public function setRecordStatus(\App\Lib\Domain\Entities\RecordStatuses $recordStatus = null)
    {
        $this->recordStatus = $recordStatus;

        return $this;
    }

    /**
     * Get recordStatus
     *
     * @return \App\Lib\Domain\Entities\RecordStatuses
     */
    public function getRecordStatus()
    {
        return $this->recordStatus;
    }

    /**
     * Set workflowStatus
     *
     * @param \App\Lib\Domain\Entities\WorkflowStatuses $workflowStatus
     *
     * @return Books
     */
    public function setWorkflowStatus(\App\Lib\Domain\Entities\WorkflowStatuses $workflowStatus = null)
    {
        $this->workflowStatus = $workflowStatus;

        return $this;
    }

    /**
     * Get workflowStatus
     *
     * @return \App\Lib\Domain\Entities\WorkflowStatuses
     */
    public function getWorkflowStatus()
    {
        return $this->workflowStatus;
    }

    /**
     * Add category
     *
     * @param \App\Lib\Domain\Entities\Categories $category
     *
     * @return Books
     */
    public function addCategory(\App\Lib\Domain\Entities\Categories $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \App\Lib\Domain\Entities\Categories $category
     */
    public function removeCategory(\App\Lib\Domain\Entities\Categories $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add genre
     *
     * @param \App\Lib\Domain\Entities\Genres $genre
     *
     * @return Books
     */
    public function addGenre(\App\Lib\Domain\Entities\Genres $genre)
    {
        $this->genre[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \App\Lib\Domain\Entities\Genres $genre
     */
    public function removeGenre(\App\Lib\Domain\Entities\Genres $genre)
    {
        $this->genre->removeElement($genre);
    }

    /**
     * Get genre
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Add tag
     *
     * @param \App\Lib\Domain\Entities\Tags $tag
     *
     * @return Books
     */
    public function addTag(\App\Lib\Domain\Entities\Tags $tag)
    {
        $this->tag[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \App\Lib\Domain\Entities\Tags $tag
     */
    public function removeTag(\App\Lib\Domain\Entities\Tags $tag)
    {
        $this->tag->removeElement($tag);
    }

    /**
     * Get tag
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTag()
    {
        return $this->tag;
    }
}

