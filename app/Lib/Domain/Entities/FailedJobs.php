<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * FailedJobs
 *
 * @ORM\Table(name="failed_jobs")
 * @ORM\Entity
 */
class FailedJobs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="failed_at", type="datetime", nullable=false)
     */
    private $failedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="connection", type="string", length=255, nullable=false)
     */
    private $connection;

    /**
     * @var string
     *
     * @ORM\Column(name="queue", type="string", length=255, nullable=false)
     */
    private $queue;

    /**
     * @var string
     *
     * @ORM\Column(name="payload", type="text", length=65535, nullable=false)
     */
    private $payload;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set failedAt
     *
     * @param \DateTime $failedAt
     *
     * @return FailedJobs
     */
    public function setFailedAt($failedAt)
    {
        $this->failedAt = $failedAt;

        return $this;
    }

    /**
     * Get failedAt
     *
     * @return \DateTime
     */
    public function getFailedAt()
    {
        return $this->failedAt;
    }

    /**
     * Set connection
     *
     * @param string $connection
     *
     * @return FailedJobs
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;

        return $this;
    }

    /**
     * Get connection
     *
     * @return string
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Set queue
     *
     * @param string $queue
     *
     * @return FailedJobs
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;

        return $this;
    }

    /**
     * Get queue
     *
     * @return string
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * Set payload
     *
     * @param string $payload
     *
     * @return FailedJobs
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get payload
     *
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }
}

