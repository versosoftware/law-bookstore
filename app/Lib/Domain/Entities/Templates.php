<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Templates
 *
 * @ORM\Table(name="templates", indexes={@ORM\Index(name="layouts_templates_fk", columns={"layout_id"}), @ORM\Index(name="organisations_templates_fk", columns={"organisation_id"})})
 * @ORM\Entity
 */
class Templates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", length=65535, nullable=false)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="string", length=150, nullable=true)
     */
    private $summary;

    /**
     * @var \App\Lib\Domain\Entities\Layouts
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Layouts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="layout_id", referencedColumnName="id")
     * })
     */
    private $layout;

    /**
     * @var \App\Lib\Domain\Entities\Organisations
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Organisations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisation_id", referencedColumnName="id")
     * })
     */
    private $organisation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Templates
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Templates
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Templates
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Templates
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set layout
     *
     * @param \App\Lib\Domain\Entities\Layouts $layout
     *
     * @return Templates
     */
    public function setLayout(\App\Lib\Domain\Entities\Layouts $layout = null)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * Get layout
     *
     * @return \App\Lib\Domain\Entities\Layouts
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set organisation
     *
     * @param \App\Lib\Domain\Entities\Organisations $organisation
     *
     * @return Templates
     */
    public function setOrganisation(\App\Lib\Domain\Entities\Organisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App\Lib\Domain\Entities\Organisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }
}

