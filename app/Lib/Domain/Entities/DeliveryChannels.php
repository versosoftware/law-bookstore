<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeliveryChannels
 *
 * @ORM\Table(name="delivery_channels", indexes={@ORM\Index(name="delivery_channel_types_delivery_chanels_fk", columns={"delivery_channel_type_id"}), @ORM\Index(name="applications_delivery_channels_fk", columns={"application_id"}), @ORM\Index(name="books_delivery_channels_fk", columns={"book_id"})})
 * @ORM\Entity
 */
class DeliveryChannels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var \App\Lib\Domain\Entities\Applications
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Applications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     * })
     */
    private $application;

    /**
     * @var \App\Lib\Domain\Entities\Books
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Books")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * })
     */
    private $book;

    /**
     * @var \App\Lib\Domain\Entities\DeliveryChannelTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\DeliveryChannelTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_channel_type_id", referencedColumnName="id")
     * })
     */
    private $deliveryChannelType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\PaymentOptions", inversedBy="deliveryChannel")
     * @ORM\JoinTable(name="delivery_channels_payment_options",
     *   joinColumns={
     *     @ORM\JoinColumn(name="delivery_channel", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="payment_options_id", referencedColumnName="id")
     *   }
     * )
     */
    private $paymentOptions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paymentOptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DeliveryChannels
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DeliveryChannels
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return DeliveryChannels
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DeliveryChannels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return DeliveryChannels
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set application
     *
     * @param \App\Lib\Domain\Entities\Applications $application
     *
     * @return DeliveryChannels
     */
    public function setApplication(\App\Lib\Domain\Entities\Applications $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \App\Lib\Domain\Entities\Applications
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set book
     *
     * @param \App\Lib\Domain\Entities\Books $book
     *
     * @return DeliveryChannels
     */
    public function setBook(\App\Lib\Domain\Entities\Books $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \App\Lib\Domain\Entities\Books
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set deliveryChannelType
     *
     * @param \App\Lib\Domain\Entities\DeliveryChannelTypes $deliveryChannelType
     *
     * @return DeliveryChannels
     */
    public function setDeliveryChannelType(\App\Lib\Domain\Entities\DeliveryChannelTypes $deliveryChannelType = null)
    {
        $this->deliveryChannelType = $deliveryChannelType;

        return $this;
    }

    /**
     * Get deliveryChannelType
     *
     * @return \App\Lib\Domain\Entities\DeliveryChannelTypes
     */
    public function getDeliveryChannelType()
    {
        return $this->deliveryChannelType;
    }

    /**
     * Add paymentOption
     *
     * @param \App\Lib\Domain\Entities\PaymentOptions $paymentOption
     *
     * @return DeliveryChannels
     */
    public function addPaymentOption(\App\Lib\Domain\Entities\PaymentOptions $paymentOption)
    {
        $this->paymentOptions[] = $paymentOption;

        return $this;
    }

    /**
     * Remove paymentOption
     *
     * @param \App\Lib\Domain\Entities\PaymentOptions $paymentOption
     */
    public function removePaymentOption(\App\Lib\Domain\Entities\PaymentOptions $paymentOption)
    {
        $this->paymentOptions->removeElement($paymentOption);
    }

    /**
     * Get paymentOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentOptions()
    {
        return $this->paymentOptions;
    }
}

