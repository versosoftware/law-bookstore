<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Widgets
 *
 * @ORM\Table(name="widgets", indexes={@ORM\Index(name="organisations_widgets_fk", columns={"organisation_id"})})
 * @ORM\Entity
 */
class Widgets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fully_qualified_class_name", type="string", length=150, nullable=false)
     */
    private $fullyQualifiedClassName;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text", length=65535, nullable=false)
     */
    private $html;

    /**
     * @var string
     *
     * @ORM\Column(name="parameters", type="text", length=65535, nullable=true)
     */
    private $parameters;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=30, nullable=false)
     */
    private $slug;

    /**
     * @var \App\Lib\Domain\Entities\Organisations
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Organisations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisation_id", referencedColumnName="id")
     * })
     */
    private $organisation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullyQualifiedClassName
     *
     * @param string $fullyQualifiedClassName
     *
     * @return Widgets
     */
    public function setFullyQualifiedClassName($fullyQualifiedClassName)
    {
        $this->fullyQualifiedClassName = $fullyQualifiedClassName;

        return $this;
    }

    /**
     * Get fullyQualifiedClassName
     *
     * @return string
     */
    public function getFullyQualifiedClassName()
    {
        return $this->fullyQualifiedClassName;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Widgets
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Widgets
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set html
     *
     * @param string $html
     *
     * @return Widgets
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set parameters
     *
     * @param string $parameters
     *
     * @return Widgets
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get parameters
     *
     * @return string
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Widgets
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set organisation
     *
     * @param \App\Lib\Domain\Entities\Organisations $organisation
     *
     * @return Widgets
     */
    public function setOrganisation(\App\Lib\Domain\Entities\Organisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App\Lib\Domain\Entities\Organisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }
}

