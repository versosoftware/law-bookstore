<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imports
 *
 * @ORM\Table(name="imports", uniqueConstraints={@ORM\UniqueConstraint(name="media_imports_idx", columns={"process_uuid"})}, indexes={@ORM\Index(name="import_statuses_imports_fk", columns={"import_status_id"}), @ORM\Index(name="users_media_imports_fk", columns={"launched_by_user_id"})})
 * @ORM\Entity
 */
class Imports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="import_error_message", type="text", length=65535, nullable=true)
     */
    private $importErrorMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_path", type="string", length=255, nullable=true)
     */
    private $providerPath;

    /**
     * @var string
     *
     * @ORM\Column(name="rss_path", type="string", length=255, nullable=true)
     */
    private $rssPath;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var integer
     *
     * @ORM\Column(name="items_count", type="integer", nullable=false)
     */
    private $itemsCount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="naming_convention", type="string", length=150, nullable=true)
     */
    private $namingConvention;

    /**
     * @var string
     *
     * @ORM\Column(name="process_uuid", type="string", length=100, nullable=false)
     */
    private $processUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="bulk_settings", type="text", length=65535, nullable=false)
     */
    private $bulkSettings;

    /**
     * @var \App\Lib\Domain\Entities\ImportStatuses
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\ImportStatuses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="import_status_id", referencedColumnName="id")
     * })
     */
    private $importStatus;

    /**
     * @var \App\Lib\Domain\Entities\Users
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="launched_by_user_id", referencedColumnName="id")
     * })
     */
    private $launchedByUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Pages", inversedBy="import")
     * @ORM\JoinTable(name="import_items",
     *   joinColumns={
     *     @ORM\JoinColumn(name="import_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     *   }
     * )
     */
    private $page;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Imports
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set importErrorMessage
     *
     * @param string $importErrorMessage
     *
     * @return Imports
     */
    public function setImportErrorMessage($importErrorMessage)
    {
        $this->importErrorMessage = $importErrorMessage;

        return $this;
    }

    /**
     * Get importErrorMessage
     *
     * @return string
     */
    public function getImportErrorMessage()
    {
        return $this->importErrorMessage;
    }

    /**
     * Set providerPath
     *
     * @param string $providerPath
     *
     * @return Imports
     */
    public function setProviderPath($providerPath)
    {
        $this->providerPath = $providerPath;

        return $this;
    }

    /**
     * Get providerPath
     *
     * @return string
     */
    public function getProviderPath()
    {
        return $this->providerPath;
    }

    /**
     * Set rssPath
     *
     * @param string $rssPath
     *
     * @return Imports
     */
    public function setRssPath($rssPath)
    {
        $this->rssPath = $rssPath;

        return $this;
    }

    /**
     * Get rssPath
     *
     * @return string
     */
    public function getRssPath()
    {
        return $this->rssPath;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return Imports
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set itemsCount
     *
     * @param integer $itemsCount
     *
     * @return Imports
     */
    public function setItemsCount($itemsCount)
    {
        $this->itemsCount = $itemsCount;

        return $this;
    }

    /**
     * Get itemsCount
     *
     * @return integer
     */
    public function getItemsCount()
    {
        return $this->itemsCount;
    }

    /**
     * Set namingConvention
     *
     * @param string $namingConvention
     *
     * @return Imports
     */
    public function setNamingConvention($namingConvention)
    {
        $this->namingConvention = $namingConvention;

        return $this;
    }

    /**
     * Get namingConvention
     *
     * @return string
     */
    public function getNamingConvention()
    {
        return $this->namingConvention;
    }

    /**
     * Set processUuid
     *
     * @param string $processUuid
     *
     * @return Imports
     */
    public function setProcessUuid($processUuid)
    {
        $this->processUuid = $processUuid;

        return $this;
    }

    /**
     * Get processUuid
     *
     * @return string
     */
    public function getProcessUuid()
    {
        return $this->processUuid;
    }

    /**
     * Set bulkSettings
     *
     * @param string $bulkSettings
     *
     * @return Imports
     */
    public function setBulkSettings($bulkSettings)
    {
        $this->bulkSettings = $bulkSettings;

        return $this;
    }

    /**
     * Get bulkSettings
     *
     * @return string
     */
    public function getBulkSettings()
    {
        return $this->bulkSettings;
    }

    /**
     * Set importStatus
     *
     * @param \App\Lib\Domain\Entities\ImportStatuses $importStatus
     *
     * @return Imports
     */
    public function setImportStatus(\App\Lib\Domain\Entities\ImportStatuses $importStatus = null)
    {
        $this->importStatus = $importStatus;

        return $this;
    }

    /**
     * Get importStatus
     *
     * @return \App\Lib\Domain\Entities\ImportStatuses
     */
    public function getImportStatus()
    {
        return $this->importStatus;
    }

    /**
     * Set launchedByUser
     *
     * @param \App\Lib\Domain\Entities\Users $launchedByUser
     *
     * @return Imports
     */
    public function setLaunchedByUser(\App\Lib\Domain\Entities\Users $launchedByUser = null)
    {
        $this->launchedByUser = $launchedByUser;

        return $this;
    }

    /**
     * Get launchedByUser
     *
     * @return \App\Lib\Domain\Entities\Users
     */
    public function getLaunchedByUser()
    {
        return $this->launchedByUser;
    }

    /**
     * Add page
     *
     * @param \App\Lib\Domain\Entities\Pages $page
     *
     * @return Imports
     */
    public function addPage(\App\Lib\Domain\Entities\Pages $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \App\Lib\Domain\Entities\Pages $page
     */
    public function removePage(\App\Lib\Domain\Entities\Pages $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Get page
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPage()
    {
        return $this->page;
    }
}

