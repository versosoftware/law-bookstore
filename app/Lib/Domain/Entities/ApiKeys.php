<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ApiKeys
 *
 * @ORM\Table(name="api_keys", uniqueConstraints={@ORM\UniqueConstraint(name="api_keys_hash_idx", columns={"hash"})}, indexes={@ORM\Index(name="applications_api_keys_fk", columns={"application_id"})})
 * @ORM\Entity
 */
class ApiKeys
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=100, nullable=false)
     */
    private $hash;

    /**
     * @var \App\Lib\Domain\Entities\Applications
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Applications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     * })
     */
    private $application;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ApiKeys
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ApiKeys
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return ApiKeys
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set application
     *
     * @param \App\Lib\Domain\Entities\Applications $application
     *
     * @return ApiKeys
     */
    public function setApplication(\App\Lib\Domain\Entities\Applications $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \App\Lib\Domain\Entities\Applications
     */
    public function getApplication()
    {
        return $this->application;
    }
}

