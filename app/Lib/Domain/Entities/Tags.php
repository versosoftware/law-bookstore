<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tags
 *
 * @ORM\Table(name="tags", indexes={@ORM\Index(name="tag_categories_tags_fk", columns={"tag_group_id"})})
 * @ORM\Entity
 */
class Tags
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=30, nullable=false)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="counter", type="integer", nullable=true)
     */
    private $counter = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var \App\Lib\Domain\Entities\TagGroups
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\TagGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tag_group_id", referencedColumnName="id")
     * })
     */
    private $tagGroup;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Books", mappedBy="tag")
     */
    private $currentBook;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currentBook = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Tags
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     *
     * @return Tags
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Tags
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set tagGroup
     *
     * @param \App\Lib\Domain\Entities\TagGroups $tagGroup
     *
     * @return Tags
     */
    public function setTagGroup(\App\Lib\Domain\Entities\TagGroups $tagGroup = null)
    {
        $this->tagGroup = $tagGroup;

        return $this;
    }

    /**
     * Get tagGroup
     *
     * @return \App\Lib\Domain\Entities\TagGroups
     */
    public function getTagGroup()
    {
        return $this->tagGroup;
    }

    /**
     * Add currentBook
     *
     * @param \App\Lib\Domain\Entities\Books $currentBook
     *
     * @return Tags
     */
    public function addCurrentBook(\App\Lib\Domain\Entities\Books $currentBook)
    {
        $this->currentBook[] = $currentBook;

        return $this;
    }

    /**
     * Remove currentBook
     *
     * @param \App\Lib\Domain\Entities\Books $currentBook
     */
    public function removeCurrentBook(\App\Lib\Domain\Entities\Books $currentBook)
    {
        $this->currentBook->removeElement($currentBook);
    }

    /**
     * Get currentBook
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrentBook()
    {
        return $this->currentBook;
    }
}

