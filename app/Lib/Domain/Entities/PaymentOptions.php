<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentOptions
 *
 * @ORM\Table(name="payment_options", indexes={@ORM\Index(name="payment_types_delivery_channel_payment_options_fk", columns={"payment_type_id"}), @ORM\Index(name="books_payment_options_fk", columns={"book_id"})})
 * @ORM\Entity
 */
class PaymentOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="consumer_message", type="string", length=400, nullable=false)
     */
    private $consumerMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="renew_message", type="string", length=400, nullable=true)
     */
    private $renewMessage;

    /**
     * @var \App\Lib\Domain\Entities\Books
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Books")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * })
     */
    private $book;

    /**
     * @var \App\Lib\Domain\Entities\PaymentTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\PaymentTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id")
     * })
     */
    private $paymentType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Chapters", mappedBy="paymentOption")
     */
    private $chapter;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\DeliveryChannels", mappedBy="paymentOptions")
     */
    private $deliveryChannel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chapter = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deliveryChannel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentOptions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set consumerMessage
     *
     * @param string $consumerMessage
     *
     * @return PaymentOptions
     */
    public function setConsumerMessage($consumerMessage)
    {
        $this->consumerMessage = $consumerMessage;

        return $this;
    }

    /**
     * Get consumerMessage
     *
     * @return string
     */
    public function getConsumerMessage()
    {
        return $this->consumerMessage;
    }

    /**
     * Set renewMessage
     *
     * @param string $renewMessage
     *
     * @return PaymentOptions
     */
    public function setRenewMessage($renewMessage)
    {
        $this->renewMessage = $renewMessage;

        return $this;
    }

    /**
     * Get renewMessage
     *
     * @return string
     */
    public function getRenewMessage()
    {
        return $this->renewMessage;
    }

    /**
     * Set book
     *
     * @param \App\Lib\Domain\Entities\Books $book
     *
     * @return PaymentOptions
     */
    public function setBook(\App\Lib\Domain\Entities\Books $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \App\Lib\Domain\Entities\Books
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set paymentType
     *
     * @param \App\Lib\Domain\Entities\PaymentTypes $paymentType
     *
     * @return PaymentOptions
     */
    public function setPaymentType(\App\Lib\Domain\Entities\PaymentTypes $paymentType = null)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return \App\Lib\Domain\Entities\PaymentTypes
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Add chapter
     *
     * @param \App\Lib\Domain\Entities\Chapters $chapter
     *
     * @return PaymentOptions
     */
    public function addChapter(\App\Lib\Domain\Entities\Chapters $chapter)
    {
        $this->chapter[] = $chapter;

        return $this;
    }

    /**
     * Remove chapter
     *
     * @param \App\Lib\Domain\Entities\Chapters $chapter
     */
    public function removeChapter(\App\Lib\Domain\Entities\Chapters $chapter)
    {
        $this->chapter->removeElement($chapter);
    }

    /**
     * Get chapter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Add deliveryChannel
     *
     * @param \App\Lib\Domain\Entities\DeliveryChannels $deliveryChannel
     *
     * @return PaymentOptions
     */
    public function addDeliveryChannel(\App\Lib\Domain\Entities\DeliveryChannels $deliveryChannel)
    {
        $this->deliveryChannel[] = $deliveryChannel;

        return $this;
    }

    /**
     * Remove deliveryChannel
     *
     * @param \App\Lib\Domain\Entities\DeliveryChannels $deliveryChannel
     */
    public function removeDeliveryChannel(\App\Lib\Domain\Entities\DeliveryChannels $deliveryChannel)
    {
        $this->deliveryChannel->removeElement($deliveryChannel);
    }

    /**
     * Get deliveryChannel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeliveryChannel()
    {
        return $this->deliveryChannel;
    }
}

