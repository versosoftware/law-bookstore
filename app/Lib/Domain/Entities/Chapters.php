<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chapters
 *
 * @ORM\Table(name="chapters", indexes={@ORM\Index(name="pricing_types_chapters_fk", columns={"pricing_type_id"}), @ORM\Index(name="books_chapters_fk", columns={"book_id"})})
 * @ORM\Entity
 */
class Chapters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="pricing_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $pricingAmount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder = '0';

    /**
     * @var \App\Lib\Domain\Entities\Books
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Books")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * })
     */
    private $book;

    /**
     * @var \App\Lib\Domain\Entities\PricingTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\PricingTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pricing_type_id", referencedColumnName="id")
     * })
     */
    private $pricingType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\PaymentOptions", inversedBy="chapter")
     * @ORM\JoinTable(name="chapter_payment_options",
     *   joinColumns={
     *     @ORM\JoinColumn(name="chapter_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="payment_option", referencedColumnName="id")
     *   }
     * )
     */
    private $paymentOption;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paymentOption = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Chapters
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pricingAmount
     *
     * @param float $pricingAmount
     *
     * @return Chapters
     */
    public function setPricingAmount($pricingAmount)
    {
        $this->pricingAmount = $pricingAmount;

        return $this;
    }

    /**
     * Get pricingAmount
     *
     * @return float
     */
    public function getPricingAmount()
    {
        return $this->pricingAmount;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Chapters
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Chapters
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Chapters
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Chapters
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Chapters
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set book
     *
     * @param \App\Lib\Domain\Entities\Books $book
     *
     * @return Chapters
     */
    public function setBook(\App\Lib\Domain\Entities\Books $book = null)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \App\Lib\Domain\Entities\Books
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set pricingType
     *
     * @param \App\Lib\Domain\Entities\PricingTypes $pricingType
     *
     * @return Chapters
     */
    public function setPricingType(\App\Lib\Domain\Entities\PricingTypes $pricingType = null)
    {
        $this->pricingType = $pricingType;

        return $this;
    }

    /**
     * Get pricingType
     *
     * @return \App\Lib\Domain\Entities\PricingTypes
     */
    public function getPricingType()
    {
        return $this->pricingType;
    }

    /**
     * Add paymentOption
     *
     * @param \App\Lib\Domain\Entities\PaymentOptions $paymentOption
     *
     * @return Chapters
     */
    public function addPaymentOption(\App\Lib\Domain\Entities\PaymentOptions $paymentOption)
    {
        $this->paymentOption[] = $paymentOption;

        return $this;
    }

    /**
     * Remove paymentOption
     *
     * @param \App\Lib\Domain\Entities\PaymentOptions $paymentOption
     */
    public function removePaymentOption(\App\Lib\Domain\Entities\PaymentOptions $paymentOption)
    {
        $this->paymentOption->removeElement($paymentOption);
    }

    /**
     * Get paymentOption
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentOption()
    {
        return $this->paymentOption;
    }
}

