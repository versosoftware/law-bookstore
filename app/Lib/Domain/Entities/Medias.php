<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medias
 *
 * @ORM\Table(name="medias", indexes={@ORM\Index(name="organisation_medias_fk", columns={"organisation_id"}), @ORM\Index(name="media_types_medias_fk", columns={"media_type_id"}), @ORM\Index(name="pages_medias_fk", columns={"page_id"})})
 * @ORM\Entity
 */
class Medias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=450, nullable=false)
     */
    private $path;

    /**
     * @var float
     *
     * @ORM\Column(name="point_of_interest_y", type="float", precision=10, scale=0, nullable=false)
     */
    private $pointOfInterestY = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="parent_width", type="string", length=5, nullable=true)
     */
    private $parentWidth;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=20, nullable=false)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_height", type="string", length=5, nullable=true)
     */
    private $parentHeight;

    /**
     * @var float
     *
     * @ORM\Column(name="point_of_interest_x", type="float", precision=10, scale=0, nullable=false)
     */
    private $pointOfInterestX = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail_path", type="string", length=450, nullable=true)
     */
    private $thumbnailPath;

    /**
     * @var string
     *
     * @ORM\Column(name="medium_path", type="string", length=450, nullable=true)
     */
    private $mediumPath;

    /**
     * @var \App\Lib\Domain\Entities\MediaTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\MediaTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="media_type_id", referencedColumnName="id")
     * })
     */
    private $mediaType;

    /**
     * @var \App\Lib\Domain\Entities\Organisations
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Organisations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisation_id", referencedColumnName="id")
     * })
     */
    private $organisation;

    /**
     * @var \App\Lib\Domain\Entities\Pages
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Pages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     * })
     */
    private $page;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Medias
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Medias
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set pointOfInterestY
     *
     * @param float $pointOfInterestY
     *
     * @return Medias
     */
    public function setPointOfInterestY($pointOfInterestY)
    {
        $this->pointOfInterestY = $pointOfInterestY;

        return $this;
    }

    /**
     * Get pointOfInterestY
     *
     * @return float
     */
    public function getPointOfInterestY()
    {
        return $this->pointOfInterestY;
    }

    /**
     * Set parentWidth
     *
     * @param string $parentWidth
     *
     * @return Medias
     */
    public function setParentWidth($parentWidth)
    {
        $this->parentWidth = $parentWidth;

        return $this;
    }

    /**
     * Get parentWidth
     *
     * @return string
     */
    public function getParentWidth()
    {
        return $this->parentWidth;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Medias
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set parentHeight
     *
     * @param string $parentHeight
     *
     * @return Medias
     */
    public function setParentHeight($parentHeight)
    {
        $this->parentHeight = $parentHeight;

        return $this;
    }

    /**
     * Get parentHeight
     *
     * @return string
     */
    public function getParentHeight()
    {
        return $this->parentHeight;
    }

    /**
     * Set pointOfInterestX
     *
     * @param float $pointOfInterestX
     *
     * @return Medias
     */
    public function setPointOfInterestX($pointOfInterestX)
    {
        $this->pointOfInterestX = $pointOfInterestX;

        return $this;
    }

    /**
     * Get pointOfInterestX
     *
     * @return float
     */
    public function getPointOfInterestX()
    {
        return $this->pointOfInterestX;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Medias
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Medias
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set thumbnailPath
     *
     * @param string $thumbnailPath
     *
     * @return Medias
     */
    public function setThumbnailPath($thumbnailPath)
    {
        $this->thumbnailPath = $thumbnailPath;

        return $this;
    }

    /**
     * Get thumbnailPath
     *
     * @return string
     */
    public function getThumbnailPath()
    {
        return $this->thumbnailPath;
    }

    /**
     * Set mediumPath
     *
     * @param string $mediumPath
     *
     * @return Medias
     */
    public function setMediumPath($mediumPath)
    {
        $this->mediumPath = $mediumPath;

        return $this;
    }

    /**
     * Get mediumPath
     *
     * @return string
     */
    public function getMediumPath()
    {
        return $this->mediumPath;
    }

    /**
     * Set mediaType
     *
     * @param \App\Lib\Domain\Entities\MediaTypes $mediaType
     *
     * @return Medias
     */
    public function setMediaType(\App\Lib\Domain\Entities\MediaTypes $mediaType = null)
    {
        $this->mediaType = $mediaType;

        return $this;
    }

    /**
     * Get mediaType
     *
     * @return \App\Lib\Domain\Entities\MediaTypes
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * Set organisation
     *
     * @param \App\Lib\Domain\Entities\Organisations $organisation
     *
     * @return Medias
     */
    public function setOrganisation(\App\Lib\Domain\Entities\Organisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return \App\Lib\Domain\Entities\Organisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set page
     *
     * @param \App\Lib\Domain\Entities\Pages $page
     *
     * @return Medias
     */
    public function setPage(\App\Lib\Domain\Entities\Pages $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \App\Lib\Domain\Entities\Pages
     */
    public function getPage()
    {
        return $this->page;
    }
}

