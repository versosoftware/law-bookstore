<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pages
 *
 * @ORM\Table(name="pages", uniqueConstraints={@ORM\UniqueConstraint(name="contents_idx", columns={"url"})}, indexes={@ORM\Index(name="pages_pages_fk", columns={"featured_image_page_id"}), @ORM\Index(name="pages_pages_media_fk", columns={"page_media_id"}), @ORM\Index(name="medias_pages_fk", columns={"media_id"})})
 * @ORM\Entity
 */
class Pages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", length=65535, nullable=false)
     */
    private $keywords;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     */
    private $title;

    /**
     * @var \App\Lib\Domain\Entities\Medias
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Medias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * })
     */
    private $media;

    /**
     * @var \App\Lib\Domain\Entities\Pages
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Pages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="featured_image_page_id", referencedColumnName="id")
     * })
     */
    private $featuredImagePage;

    /**
     * @var \App\Lib\Domain\Entities\Pages
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Pages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="page_media_id", referencedColumnName="id")
     * })
     */
    private $pageMedia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Imports", mappedBy="page")
     */
    private $import;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->import = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Pages
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Pages
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Pages
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Pages
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Pages
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pages
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Pages
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Pages
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set media
     *
     * @param \App\Lib\Domain\Entities\Medias $media
     *
     * @return Pages
     */
    public function setMedia(\App\Lib\Domain\Entities\Medias $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \App\Lib\Domain\Entities\Medias
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set featuredImagePage
     *
     * @param \App\Lib\Domain\Entities\Pages $featuredImagePage
     *
     * @return Pages
     */
    public function setFeaturedImagePage(\App\Lib\Domain\Entities\Pages $featuredImagePage = null)
    {
        $this->featuredImagePage = $featuredImagePage;

        return $this;
    }

    /**
     * Get featuredImagePage
     *
     * @return \App\Lib\Domain\Entities\Pages
     */
    public function getFeaturedImagePage()
    {
        return $this->featuredImagePage;
    }

    /**
     * Set pageMedia
     *
     * @param \App\Lib\Domain\Entities\Pages $pageMedia
     *
     * @return Pages
     */
    public function setPageMedia(\App\Lib\Domain\Entities\Pages $pageMedia = null)
    {
        $this->pageMedia = $pageMedia;

        return $this;
    }

    /**
     * Get pageMedia
     *
     * @return \App\Lib\Domain\Entities\Pages
     */
    public function getPageMedia()
    {
        return $this->pageMedia;
    }

    /**
     * Add import
     *
     * @param \App\Lib\Domain\Entities\Imports $import
     *
     * @return Pages
     */
    public function addImport(\App\Lib\Domain\Entities\Imports $import)
    {
        $this->import[] = $import;

        return $this;
    }

    /**
     * Remove import
     *
     * @param \App\Lib\Domain\Entities\Imports $import
     */
    public function removeImport(\App\Lib\Domain\Entities\Imports $import)
    {
        $this->import->removeElement($import);
    }

    /**
     * Get import
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImport()
    {
        return $this->import;
    }
}

