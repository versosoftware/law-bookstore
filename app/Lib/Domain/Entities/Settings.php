<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="settings", indexes={@ORM\Index(name="payment_options_settings_fk", columns={"payment_option_id"})})
 * @ORM\Entity
 */
class Settings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_name", type="string", length=80, nullable=false)
     */
    private $settingName;

    /**
     * @var string
     *
     * @ORM\Column(name="setting_value", type="string", length=200, nullable=false)
     */
    private $settingValue;

    /**
     * @var \App\Lib\Domain\Entities\PaymentOptions
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\PaymentOptions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_option_id", referencedColumnName="id")
     * })
     */
    private $paymentOption;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set settingName
     *
     * @param string $settingName
     *
     * @return Settings
     */
    public function setSettingName($settingName)
    {
        $this->settingName = $settingName;

        return $this;
    }

    /**
     * Get settingName
     *
     * @return string
     */
    public function getSettingName()
    {
        return $this->settingName;
    }

    /**
     * Set settingValue
     *
     * @param string $settingValue
     *
     * @return Settings
     */
    public function setSettingValue($settingValue)
    {
        $this->settingValue = $settingValue;

        return $this;
    }

    /**
     * Get settingValue
     *
     * @return string
     */
    public function getSettingValue()
    {
        return $this->settingValue;
    }

    /**
     * Set paymentOption
     *
     * @param \App\Lib\Domain\Entities\PaymentOptions $paymentOption
     *
     * @return Settings
     */
    public function setPaymentOption(\App\Lib\Domain\Entities\PaymentOptions $paymentOption = null)
    {
        $this->paymentOption = $paymentOption;

        return $this;
    }

    /**
     * Get paymentOption
     *
     * @return \App\Lib\Domain\Entities\PaymentOptions
     */
    public function getPaymentOption()
    {
        return $this->paymentOption;
    }
}

