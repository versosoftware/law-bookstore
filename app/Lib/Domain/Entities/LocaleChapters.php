<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * LocaleChapters
 *
 * @ORM\Table(name="locale_chapters", indexes={@ORM\Index(name="locales_locale_chapters_fk", columns={"locale_id"}), @ORM\Index(name="chapters_locale_chapters_fk", columns={"chapter_id"})})
 * @ORM\Entity
 */
class LocaleChapters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var \App\Lib\Domain\Entities\Chapters
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Chapters")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chapter_id", referencedColumnName="id")
     * })
     */
    private $chapter;

    /**
     * @var \App\Lib\Domain\Entities\Locales
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Locales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     * })
     */
    private $locale;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LocaleChapters
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LocaleChapters
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return LocaleChapters
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set chapter
     *
     * @param \App\Lib\Domain\Entities\Chapters $chapter
     *
     * @return LocaleChapters
     */
    public function setChapter(\App\Lib\Domain\Entities\Chapters $chapter = null)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * Get chapter
     *
     * @return \App\Lib\Domain\Entities\Chapters
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Set locale
     *
     * @param \App\Lib\Domain\Entities\Locales $locale
     *
     * @return LocaleChapters
     */
    public function setLocale(\App\Lib\Domain\Entities\Locales $locale = null)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return \App\Lib\Domain\Entities\Locales
     */
    public function getLocale()
    {
        return $this->locale;
    }
}

