<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organisations
 *
 * @ORM\Table(name="organisations", uniqueConstraints={@ORM\UniqueConstraint(name="organisation_idx", columns={"slug"})}, indexes={@ORM\Index(name="organisation_type_organisation_fk", columns={"organisation_type_id"}), @ORM\Index(name="organisation_organisation_fk", columns={"parent_organisation_id"})})
 * @ORM\Entity
 */
class Organisations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=30, nullable=true)
     */
    private $website;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=80, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=50, nullable=false)
     */
    private $slug;

    /**
     * @var \App\Lib\Domain\Entities\Organisations
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\Organisations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_organisation_id", referencedColumnName="id")
     * })
     */
    private $parentOrganisation;

    /**
     * @var \App\Lib\Domain\Entities\OrganisationTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Lib\Domain\Entities\OrganisationTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="organisation_type_id", referencedColumnName="id")
     * })
     */
    private $organisationType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Organisations
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Organisations
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Organisations
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Organisations
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Organisations
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Organisations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Organisations
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set parentOrganisation
     *
     * @param \App\Lib\Domain\Entities\Organisations $parentOrganisation
     *
     * @return Organisations
     */
    public function setParentOrganisation(\App\Lib\Domain\Entities\Organisations $parentOrganisation = null)
    {
        $this->parentOrganisation = $parentOrganisation;

        return $this;
    }

    /**
     * Get parentOrganisation
     *
     * @return \App\Lib\Domain\Entities\Organisations
     */
    public function getParentOrganisation()
    {
        return $this->parentOrganisation;
    }

    /**
     * Set organisationType
     *
     * @param \App\Lib\Domain\Entities\OrganisationTypes $organisationType
     *
     * @return Organisations
     */
    public function setOrganisationType(\App\Lib\Domain\Entities\OrganisationTypes $organisationType = null)
    {
        $this->organisationType = $organisationType;

        return $this;
    }

    /**
     * Get organisationType
     *
     * @return \App\Lib\Domain\Entities\OrganisationTypes
     */
    public function getOrganisationType()
    {
        return $this->organisationType;
    }
}

