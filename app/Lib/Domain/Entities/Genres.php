<?php

namespace App\Lib\Domain\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genres
 *
 * @ORM\Table(name="genres")
 * @ORM\Entity
 */
class Genres
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=30, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Lib\Domain\Entities\Books", mappedBy="genre")
     */
    private $currentBook;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currentBook = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Genres
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Genres
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Genres
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add currentBook
     *
     * @param \App\Lib\Domain\Entities\Books $currentBook
     *
     * @return Genres
     */
    public function addCurrentBook(\App\Lib\Domain\Entities\Books $currentBook)
    {
        $this->currentBook[] = $currentBook;

        return $this;
    }

    /**
     * Remove currentBook
     *
     * @param \App\Lib\Domain\Entities\Books $currentBook
     */
    public function removeCurrentBook(\App\Lib\Domain\Entities\Books $currentBook)
    {
        $this->currentBook->removeElement($currentBook);
    }

    /**
     * Get currentBook
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrentBook()
    {
        return $this->currentBook;
    }
}

